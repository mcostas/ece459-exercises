fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32
{
    let multiples_of_1: Vec<i32> = find_multiples(number, multiple1);
    let multiples_of_2: Vec<i32> = find_multiples(number, multiple2);
    let mut all_multiples: Vec<i32> = multiples_of_1;
    for current_multiple in multiples_of_2{
        if !all_multiples.iter().any(|&i| i == current_multiple){
            all_multiples.push(current_multiple);
        }
    }
    let sum = all_multiples.iter().sum();
    sum
}

fn find_multiples(max_num: i32, base_num: i32) -> Vec<i32>{
    let mut current_num = base_num;
    let mut multiples: Vec<i32> = Vec::new();
    while current_num < max_num{
        multiples.push(current_num);
        current_num = current_num + base_num;
    }
    multiples
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}