use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let tx1 = mpsc::Sender::clone(&tx);
    thread::spawn(move || {
        let val = String::from("First Message!");
        tx.send(val).unwrap();
    });
    thread::spawn(move || {
        let val = String::from("Second Cooler Message!");
        thread::sleep(Duration::from_secs(1));
        tx1.send(val).unwrap();
    });
    
    for received in rx {
        println!("Got: {}", received);
    }
}
