use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    let mut i = 0;
    while i < N{
        children.push(thread::spawn(||{
            println!("Yo, Angelo");
        }));
        i += 1;
    }
    for child in children{
        child.join().unwrap();
    }
}
