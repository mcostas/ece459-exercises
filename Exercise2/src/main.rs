// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut x = 1;
    let mut pre = 0;
    let mut fib = 1;
    if n < 1{
        fib = pre;
    }
    else if n > 1{
        while x < n{
            let _temp = fib;
            fib = pre + fib;
            pre = _temp;
            x += 1;
        }   
    }
    fib
}

fn main() {
    println!("{}", fibonacci_number(10));
}
